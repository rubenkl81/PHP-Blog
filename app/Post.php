<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Log;

class Post extends Model
{
    //
    function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    function comments(){
        return $this->hasMany('App\Comment');
    }
    function attachments(){
        return $this->hasMany('App\Attachment');
    }

    function votes() {
        return $this-hasMany('App\PostVote','post_id');
    }

    function getVoteCount() {
        $votes = PostVote::where('post_id','=',$this->id)->get();

        $votecount = 0;
        foreach ($votes as $vote) {
            if ($vote->is_positive) {
                $votecount++;
            } else {
                $votecount--;
            }
        }

        return $votecount;
    }

    function getMarkdown() {
	return Markdown::convertToHtml($this->postdata);
    }

}
