<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
*/

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'BlogController@showIndex')->name('index');
    Route::get('/search', 'BlogController@getSearch');
    Route::get('/date', 'BlogController@getDate');

    Route::resource('post','PostController');
    Route::post('/post/{id}/vote', ["uses" => 'PostController@vote', "as" => "post.vote"]);
    Route::post('/comment/{id}/vote', ["uses" => 'CommentController@vote', "as" => "comment.vote"]);

    Route::resource('comment','CommentController');
    Route::resource('attachment','AttachmentController');

    Route::controller('gallery', 'GalleryController');

    Route::controller('user', 'UserController');

});

Route::auth();
Route::get('/home', 'HomeController@index');
