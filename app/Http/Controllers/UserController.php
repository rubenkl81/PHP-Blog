<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class UserController extends Controller
{
    //
    public function getActivate(Request $request) {

        if (!$request->has('activation_code')) {
            Log::info('no activation code supplied!');
            return redirect('/');
        }
        
        $user = User::where('activation_code','=',$request->activation_code)->first();

        if (!$user) {
            Log::info('no user found with activation code: '.$request->activation_code);
            return redirect('/');
        }

        $user->email_activated = true;
        $user->save();

        return redirect('/');
    }
}
