<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use \Validator;
use \Auth;

use App\Post;
use App\User;
use App\Comment;
use App\Attachment;
use App\PostVote;
use App\CommentVote;
use App\LogEntry;

use Log;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()){
	    session(["message" => "You must be logged in to comment"]);
            return redirect('/');
        }

        $validator = Validator::make($request->all(), [
           "comment" => "required|max:255",
        ]);

        if ($validator->fails()){
            session(["errors" => $validator->messages()]);
            return redirect('/');
        }
        
        $comment = new Comment;
        $comment->text = PostController::strip_scripts($request->comment);
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $request->post_id;

        if ($request->has('comment_id')) {
            $comment->parent_comment = $request->comment_id;
        }

        $comment->save();

        Log::info($request);

        return redirect()->route('post.show', [$request->post_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function vote(Request $request, $id) {
        if (!Auth::check()){
	    return response()->json(["vote_succeded" => false, "errors" => 
"You must be logged in to vote"]);
        }

        $validator = Validator::make($request->all(), [
           "vote" => "required|boolean"
        ]);

        if ($validator->fails()){
	    return response()->json(["vote_succeded" => false, "errors" => $validator->messages()]);
        }

        // check if user has already voted for this comment

        $votes = CommentVote::where('comment_id',$id)->where('user_id',Auth::user()->id)->get();

	if (count($votes) > 0) {
	    if ($votes->first()->is_positive == $request->vote) {
		/* return response()->json(["vote_succeded" => false, "errors" => "You have already voted on that"]);*/
		$vote = $votes->first();
		$response_type = 2;
		$vote->delete();
	    } else {
		$vote = $votes->first();
		$response_type = 3;
		$vote->is_positive = $request->vote;
		$vote->save();
	    }
	} else {
	    $vote = new CommentVote;
	    $vote->comment_id = $id;
	    $vote->user_id = Auth::user()->id;
	    $vote->is_positive = $request->vote;
	    $vote->save();
	    $response_type = 1;
	}

        return response()->json(["vote_succeded" => true, "response_type" => $response_type]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $comment = Comment::find($id);

        if(!$comment) {
            session(["message" => "Comment with id:".$request->comment_id." not found"]);
            return redirect('/');
        }

        if(!(Auth::check() and (Auth::user()->is_admin or Auth::user()->id == $comment->user_id))) {
            session(["message" => "You are not allowed to do that"]);
            return redirect('/');
        }

        $comment->is_deleted = true;
        $comment->save();

        $log = new LogEntry;
        $log->action = "Admin deleted comment with id: ".$request->id." from post with id: ".$request->post_id.", comment was: ".$request->text;
        $log->user_id = Auth::user()->id;
        $log->save();

        return redirect('/');
    }
}
