<?php

namespace App\Http\Controllers\Auth;

use Mail;
use Log;
use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller {
    /*
       |--------------------------------------------------------------------------
       | Registration & Login Controller
       |--------------------------------------------------------------------------
       |
       | This controller handles the registration of new users, as well as the
       | authentication of existing users. By default, this controller uses
       | a simple trait to add these behaviors. Why don't you explore it?
       |
     */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    // Overwrite default login method
    public function login(Request $request) {
        // check form data exists
        $this->validateLogin($request);

        // block user if too many login attempts
        $throttles = $this->isUsingThrottlesLoginsTrait();
        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // Attempt login with email

        $credentials = $this->getCredentials($request);
        $credentialsUname = $credentials;
        $credentialsUname['name'] = $credentialsUname['email'];
        unset($credentialsUname['email']);

        // check if email is activated
        $activated = false;
        $user = User::where('email','=',$credentials['email'])->orWhere('name','=',$credentialsUname['name'])->first();
        if ($user) {
            $activated = $user->email_activated;
        }

        //attempt login if actiavted
        if ($activated and Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        } elseif ($activated and Auth::guard($this->getGuard())->attempt($credentialsUname, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    // clone of default registration function, without logging user in afterwards
    protected function register(Request $request) {
	$validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

	$this->create($request->all());

	return redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
	$activation_code = md5(rand(1,PHP_INT_MAX));
	$user = new User;
	$user->name = $data['name'];
	$user->email = $data['email'];
	$user->password = bcrypt($data['password']);
	$user->activation_code = $activation_code;
	$user->is_admin = false;
	$user->email_activated = false;
	$user->save();
	
        /* $user = User::create([
	   'activation_code' => $activation_code,
           'name' => $data['name'],
           'email' => $data['email'],
           'password' => bcrypt($data['password']),
           'is_admin' => false,
           'email_activated' => false,
           ]);
	 */

	$activate_link = $_SERVER['HTTP_HOST']."/user/activate?activation_code=".$activation_code;
	
        Mail::send('emails.activation',["activate_link" => $activate_link,"user" => $user], function ($msg) use ($user) {
            $msg->from('phlog@example.com', "Phlog");
            $msg->to($user->email,$user->name)->subject("Phlog account activation");
        });
        
        return $user;
    }
}
