<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use GrahamCampbell\Markdown\Facades\Markdown;
use Carbon\Carbon;
use \Validator;

use App\Post;


class BlogController extends Controller
{
    //
    function getSearch(Request $req){
	$posts = Post::where('postdata','LIKE',"%$req->search_query%")
		     ->orWhere('title','LIKE',"%$req->search_query%")
		     ->orderBy('created_at','desc')
		     ->paginate(5);

	$years = $this->get_archive_map();

	return view('index', ['blog_posts' => $posts, "years" => $years]);
    }
    
    function getDate(Request $req) {

	$validator = Validator::make($req->all(), [
	    "year" => "required|numeric",
	    "month" => "required|numeric"
	]);

	if ($validator->fails()) {
	    session(["message" => "you must specify a month and year to load, did you not use the archive view?"]);
	    return redirect('/');
	}
	
	$posts = Post::whereRaw('YEAR(created_at) = ?',[$req->year])->whereRaw('MONTH(created_at) = ?',[$req->month])->paginate(5);

	$years = $this->get_archive_map();
	
	return view('index', ["blog_posts" => $posts, "years" => $years]);
    }

    function showIndex(Request $req) {
	$posts = Post::orderBy('created_at','desc')->paginate(5);

	foreach ($posts as $post) {
	    $post->markdown = Markdown::convertToHtml($post->postdata);
	}

	$years = $this->get_archive_map();

	return view('index', ["blog_posts" => $posts, "years" => $years]);
    }

    function num_to_date($datenum) {
	$date = Carbon::createFromFormat('!m',$datenum);
	return $date->format('F');
    }

    function getTestMail(Request $req) {
	Mail::send('emails.test',["name" => "andreas"], function ($msg) {
	    $msg->from('sample_email@example.com', "Phlog");
	    $msg->to("andreas@northcode.no","Andreas Larsen")->subject("Test email");
	});
    }

    /**
     * @param $posts
     * @return array
     */
    public function get_archive_map()
    {
	$posts = Post::orderBy('created_at','desc')->get();

	if (count($posts) == 0) {
	    return array();
	}
	
	$years = array();

	$current_year = "";

	foreach ($posts as $post) {
	    $matches = array();

	    if (preg_match('/(\d+)-(\d+)-(\d+)\s+(\d+)\:(\d+)\:(\d+)/', $post->created_at, $matches)) {
		if ($matches[1] == $current_year) {
		    if (!in_array(array("num" => $matches[2], "name" => $this->num_to_date($matches[2])), $years[count($years) - 1]["months"])) {
			$years[count($years) - 1]["months"][] = array("num" => $matches[2], "name" => $this->num_to_date($matches[2]));
		    }
		} else {
		    $years[] = array("year" => $matches[1], "visible" => false, "months" => array(array("num" => $matches[2], "name" => $this->num_to_date($matches[2]))));
		    $current_year = $matches[1];
		}
	    }
	}

	$years[0]["visible"] = true;
	
	return $years;
    }
}
