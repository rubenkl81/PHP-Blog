<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Attachment;

class GalleryController extends Controller
{
    function getIndex(Request $req){
        $attachments = Attachment::orderBy('updated_at','desc') ->get();

        return view('gallery',['attachments'=> $attachments]);
    }

}
