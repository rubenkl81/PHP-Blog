<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use \Validator;
use \Auth;

use App\Post;
use App\User;
use App\Comment;
use App\Attachment;
use App\PostVote;
use App\LogEntry;

use Log;

use GrahamCampbell\Markdown\Facades\Markdown;

class PostController extends Controller
{
    public static function strip_scripts($input)
    {
	return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $input);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	$posts = Post::orderBy('created_at','desc')->paginate(5);

	foreach ($posts as $post) {
	    $post->markdown = Markdown::convertToHtml($post->postdata);
	}

	$years = $this->get_archive_map();

	return view('index', ["blog_posts" => $posts, "years" => $years]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check() or !Auth::user()->is_admin) {
            session(["message" => "You cannot create a new post if you are not logged in as an admin account"]);
            return Redirect::route('index');
        }

        return view('newpost', ["update" => false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check() or !Auth::user()->is_admin) {
            session(["message" => "You cannot create a new post if you are not logged in as an admin account"]);
            return redirect('/');
        }

        $validator = Validator::make($request->all(), [
            "title" => "required|max:255",
            "postdata" => "required|max:1023",
            "images.*" => "max:5000"
        ]);

        if ($validator->fails()) {
            session(["errors" => $validator->messages()]);
            return back();
        }

        $post = new Post;
        $post->title = $request->title;
        $post->postdata = PostController::strip_scripts($request->postdata);
        $post->user_id = Auth::user()->id;
        $post->views = 0;
        $post->save();

        Log::info($request);
        
        Log::info('checking for file');

        if ($request->hasFile('images')) {
            Log::info('found file "image"');

            $this->multiple_upload($request,$post);
        } else {
            Log::info('no file found!');
        }

        return redirect('/post/'.$post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        
        if (!$post) {
	    session(["message" => "Post with id: ".$id." not found"]);
            return redirect('/');
        }

        $post->increment('views');

        $comments = Comment::where('post_id','=',$post->id)->whereNull('parent_comment')->orderBy('created_at','DESC')->get();

        foreach ($comments as $comment) {
            $comment->markdown = Markdown::convertToHtml($comment->text);
        }

        $votes = PostVote::where('post_id','=',$post->id)->get();

        $post->markdown = Markdown::convertToHtml($post->postdata);
        return view('viewpost', ["blog_post" => $post, "comments" => $comments, "post_votes" => $votes]);

        $attachments = Attachment::where('post_id', '=', $post->id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if (!$post) {
            return redirect('/');
        }
        return view('newpost', ["update" => true, "blog_post" => $post]);
    }

    public function vote(Request $request, $id) {
        if (!Auth::check()){
	    return response()->json(["vote_succeded" => false, "errors" => 
		"You must be logged in to vote"]);
        }

        $validator = Validator::make($request->all(), [
            "vote" => "required|boolean"
        ]);

        if ($validator->fails()){
	    return response()->json(["vote_succeded" => false, "errors" => $validator->messages()]);
        }

        // check if user has already voted for this post

        $votes = PostVote::where('post_id',$id)->where('user_id',Auth::user()->id)->get();

	if (count($votes) > 0) {
	    if ($votes->first()->is_positive == $request->vote) {
		/* return response()->json(["vote_succeded" => false, "errors" => "You have already voted on that"]);*/
		$vote = $votes->first();
		$vote->delete();
		$response_type = 2;
	    } else {
		$vote = $votes->first();
		$vote->is_positive = $request->vote;
		$vote->save();
		$response_type = 3;
	    }
	} else {
	    $vote = new PostVote;
	    $vote->post_id = $id;
	    $vote->user_id = Auth::user()->id;
	    $vote->is_positive = $request->vote;
	    $vote->save();
	    $response_type = 1;
	}

        return response()->json(["vote_succeded" => true, "response_type" => $response_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "title" => "required|max:255",
            "postdata" => "required|max:1023"
        ]);

        if ($validator->fails()){
            session(["errors" => $validator->message()]);
            return redirect('/');
        }

        $post = Post::find($id);
	if (!$post) {
	    session(["message" => "Cannot find post to update"]);
	    return back();
	}

        $post->title = $request->title;
        $post->postdata = PostController::strip_scripts($request->postdata);

        $post->save();

	if ($request->has('attachment_ids')) {

	    $attachments = $post->attachments;

	    for ($i = 0; $i < count($request->attachment_ids); $i++) {
		foreach ($attachments as $attachment) {
		    if ($attachment->id == $request->attachment_ids[$i]) {
			$attachment->show_in_gallery = $request->input('attachments_show_in_gallery_' . $attachment->id) == "on" ? true : false;
			$attachment->save();
			break;
		    }
		}
	    }
	}

        if ($request->hasFile('images')) {
            Log::info('found file "image"');

            $this->multiple_upload($request,$post);
        }

        return redirect()->route('post.show',[$post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::check() or !Auth::user()->is_admin){
            session(["message" => "You are not permitted to do this"]);
            return redirect('/');
        }

        $post = Post::find($id);

        if(!$post) {
            session(["message" => "Post with id:".$id." not found"]);
            return redirect('/');
        }

        if(!(Auth::check() and (Auth::user()->is_admin or Auth::user()->id == $post->user_id))) {
            session(["message" => "You are not allowed to do that"]);
            return redirect('/');
        }

        $post->delete();

        $log = new LogEntry;
        $log->action = "Admin deleted post with id: ".$id;
        $log->user_id = Auth::user()->id;
        $log->save();

        return redirect('/');
    }

    public function multiple_upload(Request $req, Post $post){

        $files = Input::file('images');

        Log::info('found: '.count($files)." files");

        $destinationPath = 'attachments/';

        foreach($files as $file){

            $filename = $file->getClientOriginalName();
            $mimetype = $file->getClientMimeType();
            Log::info('file name: '.$filename.' ; mime type: '.$mimetype);
            $file->move($destinationPath, $filename);

            $attachment = new Attachment;
            $attachment->post_id = $post->id;
            $attachment->path = $destinationPath.$filename;
            $attachment->type = $mimetype;
            $attachment->show_in_gallery = true;
            $attachment->save();
        }
    }
}
