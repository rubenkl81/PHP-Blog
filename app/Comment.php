<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use GrahamCampbell\Markdown\Facades\Markdown;

class Comment extends Model
{
    //
    public function getMarkdown() {
        return Markdown::convertToHtml($this->text);
    }
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function post() {
        return $this->belongsTo('App\Post', 'post_id');
    }

    public function children() {
        return $this->hasMany('App\Comment','parent_comment');
    }
    function getVoteCount() {
        $votes = CommentVote::where('comment_id','=',$this->id)->get();

        $votecount = 0;
        foreach ($votes as $vote) {
            if ($vote->is_positive) {
                $votecount++;
            } else {
                $votecount--;
            }
        }

        return $votecount;
    }
}
