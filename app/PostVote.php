<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostVote extends Model
{
    //
    public $table = 'post_votes';
    public $timestamps = false;
}
