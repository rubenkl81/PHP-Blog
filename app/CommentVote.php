<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentVote extends Model
{
    //
    public $table = 'comment_votes';
    public $timestamps = false;
}
