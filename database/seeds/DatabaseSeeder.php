<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\User;
use App\Post;
use App\Comment;
use App\Attachment;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(AttachmentSeeder::class);
    }
}

class UserSeeder extends Seeder {

    public function run() {

        DB::table('users')->delete();
        
        User::create(
            array(
                "name" => "Andreas",
                "email" => "andreas@northcode.no",
                "password" => Hash::make("password123"),
		        "is_admin" => true,
                "email_activated" => true,
            ));
        User::create(
            array(
                "name" => "Ruben",
                "email" => "rubenkl81@hotmail.com",
                "password" => Hash::make("password234"),
		        "is_admin" => false,
                "email_activated" => true,
            ));
        User::create(
            array(
                "name" => "Ådne",
                "email" => "stressmannen@live.no",
                "password" => Hash::make("password345"),
		        "is_admin" => true,
                "email_activated" => true,
            ));
    }
}

class PostSeeder extends Seeder {
    public function run() {
        $users = User::all();

        DB::table('posts')->delete();

        $ipsums = array(
            "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
            "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you, motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?",
            "Look, just because I don't be givin' no man a foot massage don't make it right for Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll kill the motherfucker, know what I'm sayin'?",
            "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.",
            "Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.",
            "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you, motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?",
            "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.",
            "Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that's what you see at a toy store. And you must think you're in a toy store, because you're here shopping for an infant named Jeb.",
            "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.",
            "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you, motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?",
            "Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.",
            "Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.",
            "Normally, both your asses would be dead as fucking fried chicken, but you happen to pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna help you. But I can't give you this case, it don't belong to me. Besides, I've already been through too much shit this morning over this case to hand it over to your dumb ass.",
            "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you, motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?",
            "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit. ");

        for($i = 0; $i <= 14; $i++) {
	    $date = Carbon::create(2014,1,1,0,0,0);
	    $date = $date->addYears(rand(0,2))
			 ->addMonths(rand(0,11))
			 ->addDays(rand(0,31));
            Post::create(
                array(
                    "user_id" => $users[rand(0,count($users) - 1)]->id,
                    "title" => "Blog post " . $i,
                    "postdata" => $ipsums[$i],
                    "views" => rand(0,255),
		    "created_at" => $date->format('Y-m-d H:i:s')
                ));
        }
    }
}

class CommentSeeder extends Seeder {

    public function run()
    {
        DB::table('comments')->delete();

        $posts = Post::all();
        $users = User::all();

        $comments = array(
            "Well, the way they make shows is, they make one show. That show's called a pilot.","Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows."," Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.",
            "Look, just because I don't be givin' no man a foot massage don't make it right for Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the nigger talks.","Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll kill the motherfucker, know what I'm sayin'?",
            "Now that there is the Tec-9, a crappy spray gun from South Miami."," This gun is advertised as the most popular gun in American crime. Do you believe that shit?"," It actually says that in the little book that comes with it: the most popular gun in American crime. ","Like they're actually proud of that shit.",
            "Well, the way they make shows is, they make one show.","That show's called a pilot.","Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows.","Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.",
            "You think water moves fast? You should see ice. It moves like it has a mind.","Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out."," Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. ","Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. ","Nature is lethal but it doesn't hold a candle to man.",
            "My money's in that office, right? If she start giving me some bullshit about it ain't there, and we got to go someplace else and get it, I'm gonna shoot you in the head then and there.","Then I'm gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you, motherfucker.","You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?",
            "Your bones don't break, mine do."," That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. ","That's also clear. But for some reason, you and I react the exact same way to water."," We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.",
            "Do you see any Teletubbies in here?"," Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it?"," No? Well, that's what you see at a toy store. And you must think you're in a toy store, because you're here shopping for an infant named Jeb.",
            "Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine."," You don't get sick, I do. That's also clear."," But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.",
        );

        for ($i = 0; $i <= 75; $i++) {
            Comment::create(
                array(
                    "user_id" => $users[rand(0,count($users) - 1)]->id,
                    "post_id" => $posts[rand(0,count($posts) - 1)]->id,
                    "text" => $comments[rand(0,count($comments) - 1)]
                )
            );
        }
    }

}

class AttachmentSeeder extends Seeder {

    public function run()
    {
        DB::table('attachments')->delete();

	$posts = Post::all();

	$file_url = "attachments/test.txt";
	$file = public_path($file_url);

	if (File::exists($file)) {
	    File::delete($file);
	}
	
	File::put($file,"this is a test");
	
	Attachment::create(
	    array(
		"post_id" => $posts[rand(0,count($posts) - 1)]->id,
		"type" => "txt",
		"path" => $file_url,
	    )
	);
    }

}
