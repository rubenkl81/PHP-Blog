# Phlog

## INSTALLATION

### REQUIREMENTS
* Php enabled web server
* Mysql server
* Composer
* Node.js

### INSTALL STEPS
* Clone this repo or extract zip file to apache virtual host or http root directory
* Create a copy of the .env.example file to its own .env file in the root project dir
* Create a database and user and add the appropriate login info to the .env file
* run these commands in the root project dir:
  * composer install
  * php artisan migrate:reset
  * php artisan migrate
  * php artisan db:seed
  * npm install
  * npm install gulp gulp-sass
  * gulp
* Make sure the web server has write permissions to the storage and public directories (and all subdirs)
* The server should now be up and runnning, if not submit an issue to the git repo

## LOGIN INFORMATION

### DEFAULT ACCOUNTS

This repo ships 3 default accounts for editing:  
These are created every time the db:seed command is run.  

* admin accounts:
  * email: andreas@northcode.no, password: password123
  * email: stressmannen@live.no, password: password345
* regular accounts:
  * email: rubenkl81@hotmail.com, password: password234

## FILES OF INTEREST
* Database models are found in the app/ directory
* Http Controllers are found in app/Http/Controllers
* Routing info is in app/Http/routes.php
* Templates are found in resources/views
* Stylesheets are found in resources/assets/sass, these compile to public/css/app.css with gulp
* Database migrations are found in database/migrations/
* Initial database entries are found in database/seeds/DatabaseSeeder.php


## LIVE EXAMPLE

As of 2016-05-09 a live version of the site can be found at: [kark.northcode.no](http://kark.northcode.no)
