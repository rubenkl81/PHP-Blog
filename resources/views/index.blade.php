@extends("layouts.main")

@section("title")
    Best Blog Ever!
@endsection
@section("content")
    <h1>Blog posts: </h1>
    <div id="blog">
        @if(count($blog_posts) > 0)
    @foreach($blog_posts as $blog_post)

        <article class="blogpost">
            <table class="up_down_vote">
                <tr>
                    <td><a data-type="post" data-id="{{ $blog_post->id }}" class="vote upvote"></a></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <b>{{$blog_post->getVoteCount()}}</b>
                    </td>
                    <td>
                        <h2><a href="{{url("/post/".$blog_post->id)}}">{{$blog_post->title}}</a></h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a data-type="post" data-id="{{ $blog_post->id }}" class="vote downvote"></a>
                    </td>
                    <td></td>
                </tr>
            </table>
                {!! Markdown::convertToHtml(str_limit($blog_post->postdata, $limit = 300, $end = '...')) !!}
            <b>Posted by: {{$blog_post->user->name}}</b>
            <b> At: {{$blog_post->created_at}}</b>
            @if($blog_post->updated_at != $blog_post->created_at)
                <b> Updated at: {{$blog_post->updated_at}}</b>
            @endif<br>
            <b>{{$blog_post->views}} view{{$blog_post->views != 1 ? "s" : "" }}</b>
            <b> and {{count($blog_post->comments)}} comment{{ count($blog_post->comments) != 1 ? "s" : "" }}</b>
        </article>
        <hr/>
    @endforeach
        @else
            @if(app('request')->input('search_query'))
                <div class="no_content_message">There are no posts containing: {{ app('request')->input('search_query') }}</div>
            @else
                <div class="no_content_message">There is nothing here yet.</div>
            @endif
        @endif
        {!! $blog_posts->render() !!}
    </div>
    @include('partial.sidebar')

    <script type="text/javascript">
        var upvote_btns = document.getElementsByClassName('vote');
        for (i = 0; i < upvote_btns.length; i++) {
            upvote_btns[i].addEventListener('click', function(event) {
                var thingtype = event.target.getAttribute('data-type');
                var thingid = event.target.getAttribute('data-id');

                var url = (thingtype == "post" ? "/post/" : "/comment/")
                        + thingid
                        + '/vote';

                var is_upvote = /(?:^|\s)upvote(?:\s|$)/.test(event.target.className);

                var ajax = new XMLHttpRequest();
                ajax.open("POST", url, true);
                var data = new FormData();
                data.append("_token", "{{ Session::token() }}");
                data.append("vote",(is_upvote ? "1" : "0"));
                ajax.send(data);
                ajax.onload = function () {
                    var response = JSON.parse(ajax.responseText);

                    if (!response.vote_succeded) {
                        // display errors
                        var error_section = document.getElementById("errors");
                        var error_node = document.createElement("b");
                        error_node.classList.add("error");
                        error_node.innerHTML = response.errors;
                        while (error_section.firstChild) {
                            error_section.removeChild(error_section.firstChild);
                        }
                        error_section.appendChild(error_node);
                        window.scrollTo(0,0);
                    }

                    var amount = 0;
                    if (response.response_type == 1) { amount = 1; }
                    else if (response.response_type == 2) { amount = -1; }
                    else if (response.response_type == 3) { amount = 2; }
                    if (is_upvote) {
                        var vote_counter = event.target
                                .parentElement
                                .parentElement
                                .nextElementSibling
                                .childNodes[1]
                                .childNodes[1];
                        vote_counter.innerHTML = Number(vote_counter.innerHTML) + amount;

                    } else {
                        amount = amount * -1;
                        var vote_counter = event.target
                                .parentElement
                                .parentElement
                                .previousElementSibling
                                .childNodes[1]
                                .childNodes[1];
                        vote_counter.innerHTML = Number(vote_counter.innerHTML) + amount;
                    }
                }
            });
        }
    </script>
@endsection
