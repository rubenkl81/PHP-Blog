<div id="side_bar">

    <section id="archive">
	<h3>Search:</h3>
        <form id="search_form" method="get" action={{ url('/search') }}>
            <input type="text" name="search_query" placeholder="Search term">
            <input type="submit" value="Search" formaction="/search">
        </form>

	<h3>Archive:</h3>
        <ul class="years">
            @if(count($years) > 0)
            @foreach($years as $year)
                <li class="year">{{$year["year"]}}
                <ul class="month @if(!$year["visible"])hidden @endif" >
                    @foreach($year["months"] as $month)
                        <li><a href="{{ url('/date?year='.$year["year"]."&month=".$month['num']) }}">{{$month["name"]}}</a></li>
                    @endforeach
                </ul></li>
            @endforeach
            @endif
        </ul>
    </section>
</div>
<script type="text/javascript">

 var years = document.getElementsByClassName("year");

 for (i = 0; i < years.length; i++) {
     years[i].addEventListener('click', function (event) {
	 var month = event.target.childNodes[1];

	 var months = document.getElementsByClassName("month");

	 for (j = 0; j < months.length; j++) {
	     months[j].classList.add("hidden");
	 }

	 month.classList.remove("hidden");
     });
 }

</script>
