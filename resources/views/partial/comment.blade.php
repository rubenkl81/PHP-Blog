@foreach ($comments as $comment)
    <div class=comment>
	@if(!$comment->is_deleted)
	    <table>
		<tr>
		    <td>
			<table class="up_down_vote">
			    <tr>
				<td><a class="vote upvote" data-type="comment" data-id="{{$comment->id}}"><!-- &#128314 --></a></td>
			    </tr>
			    <tr>
				<td>
				    <b>{{ $comment->getVoteCount() }}</b>
				</td>
			    </tr>
			    <tr>
				<td>
				    <a class="vote downvote" data-type="comment" data-id="{{$comment->id}}"><!-- &#128315 --></a>
				</td>
			    </tr>
			</table>
		    </td>
		    <td class="comment_content">
			{!! $comment->getMarkdown() !!}
			<b>By: {{ $comment->user->name }}</b>
			<b> At: {{ $comment->created_at }}</b>
		    </td>
		</tr>
	    </table>
	    @if(Auth::check())
		<button class=new_comment data-parent="{{ $comment->id }}">Comment</button>
		@if(Auth::user()->is_admin or Auth::user()->id == $comment->user_id)
		    <button class=delete_comment data-parent="{{ $comment->id }}">Delete</button>
		@endif
	    @endif
	@else
	    <p class="comment_content">[Deleted]</p>
	@endif
	<div class=comment_area></div>

	@include('partial.comment',["comments" => $comment->children])
    </div>
@endforeach

