<div class="wrapper">
    <div id="header">
	<h1>Phlog</h1>
	<nav id="navbar">
	    <ul>
		<li><a href="{{url("/")}}">Home</a> </li>
		<li><a href="{{url("/gallery")}}">Gallery</a> </li>
		@if (Auth::check())
		    @if (Auth::user()->is_admin)
			<li><a href="{{ url('/post/create') }}">New post</a></li>
		    @else
			<li><a></a></li>
		    @endif
		    <li><a href="{{ url('/logout') }}">Logout</a></li>
		@else
		    <li><a href="{{url("/register")}}">Sign up</a> </li>
		    <li><a href="{{url("/login")}}">Login</a> </li>
		@endif
	    </ul>
	</nav>
	@if (Auth::check())
	    Logged in as: {{ Auth::user()->name }}
	@endif
    </div>
</div>
