@extends('layouts.main')

@section('title')
{{ $blog_post->title }}
@endsection

@section('content')
    <article class="blogpost">

	<table class="up_down_vote">
	    <tr>
		<td><a data-type="post" data-id="{{ $blog_post->id }}" class="vote upvote"></a></td>
		<td></td>
	    </tr>
	    <tr>
		<td>
		    <b>{{$blog_post->getVoteCount()}}</b>
		</td>
		<td>
		    <h2>{{ $blog_post->title }}</h2>
		</td>
	    </tr>
	    <tr>
		<td>
		    <a data-type="post" data-id="{{ $blog_post->id }}" class="vote downvote"></a>
		</td>
		<td></td>
	    </tr>
	</table>


	@foreach($blog_post->attachments as $attachment)
	    @if(preg_match('/image\/\w+/', $attachment->type))
		<img src="{{ url($attachment->path) }}" style="max-width: 500px;" />
	    @endif
	@endforeach
	<br/>

	@if(count($blog_post->attachments) > 0)
	    <b>Attachments:</b> <br/>
	    <ul class=attachmentlist>
		@foreach ($blog_post->attachments as $attachment)
		    <li><a href="{{ url($attachment->path) }}">{{ $attachment->path }}</a></li>
		@endforeach
	    </ul>
	@endif


	{!! $blog_post->markdown !!}
	<b>Posted at: {{ $blog_post->created_at }}</b><br>
	@if($blog_post->updated_at != $blog_post->created_at)
	    <b>Updated at: {{ $blog_post->updated_at }}</b><br>
	@endif
	<b>By: {{ $blog_post->user->name }}</b>
    </article>
    <hr/>
    @if(Auth::check())
	<button class="new_comment">comment</button>
	<div class="comment_area"> </div>
	@if(Auth::user()->is_admin)
	    <a class="edit_post" href="{{ route('post.edit',[$blog_post->id]) }}">Edit post</a>
	    <form action="{{ route('post.destroy',[$blog_post->id]) }}" method="POST">
		{!! csrf_field() !!}
		{!! method_field('DELETE') !!}
		<input type="submit" value="Delete post"/>
	    </form>
	@endif
    @endif
    @include('partial.comment',["comments" => $comments])
    <script type="text/javascript">
     var post_id = {{ $blog_post->id }};
     var new_comment_btns = document.getElementsByClassName('new_comment');
     for (i = 0; i < new_comment_btns.length; i++) {

	 new_comment_btns[i].addEventListener('click', function (event) {
	     
	     var comment_area = event.target;

	     comment_area = comment_area.nextElementSibling;

	     // find next comment area
	     while (!(/(?:^|\s)comment_area(?:\s|$)/.test(comment_area.className))) {
		 if (comment_area = comment_area.nextElementSibling) {
		     break;
		 }
	     }

	     if (comment_area.childNodes.length > 1) {
		 return;
	     }
	     // generate form
	     var comment_form = document.createElement('form');
	     comment_form.setAttribute('method','post');
	     comment_form.setAttribute('action','{{ route('comment.store') }}');
	     comment_form.setAttribute('class','newcommentform');

	     // generate form fields
	     var csrf_input = document.createElement('input');
	     csrf_input.setAttribute('type','hidden');
	     csrf_input.setAttribute('name','_token');
	     csrf_input.setAttribute('value','{{ Session::token() }}');

	     var post_id_input = document.createElement('input');
	     post_id_input.setAttribute('type','hidden');
	     post_id_input.setAttribute('name','post_id');
	     post_id_input.setAttribute('value',post_id);

	     // add comment id if set
	     console.log(event.target);
	     if (event.target.hasAttribute('data-parent')) {

		 var comment_id = document.createElement('input');
		 comment_id.setAttribute('type','hidden');
		 comment_id.setAttribute('name','comment_id');
		 comment_id.setAttribute('value',event.target.getAttribute('data-parent'));
		 comment_form.appendChild(comment_id);
	     } else {
		 console.log("no parent found!");
	     }

	     var textarea_input = document.createElement('textarea');
	     textarea_input.setAttribute('name','comment');
	     textarea_input.setAttribute('placeholder','Type a comment...');
	     var submit_input = document.createElement('input');
	     submit_input.setAttribute('type','submit');
	     submit_input.setAttribute('value','Comment');

	     comment_form.appendChild(csrf_input);
	     comment_form.appendChild(post_id_input);
	     comment_form.appendChild(textarea_input);
	     comment_form.appendChild(submit_input);

	     comment_area.appendChild(comment_form);
	 });
     }
    </script>

    <script type="text/javascript">
     var delete_comment_btns = document.getElementsByClassName('delete_comment')
     for(i = 0; i < delete_comment_btns.length; i++){
	 delete_comment_btns[i].addEventListener('click', function (event) {
	     var conf = confirm("Are you sure you want to delete this comment?");
	     if (conf){
		 var r = new XMLHttpRequest();
		 r.open("POST", "{{ url('/comment/') }}/" + event.target.getAttribute("data-parent"), true);
		 r.setRequestHeader('X-CSRF-TOKEN','{{ Session::token() }}');
		 var data=new FormData();
		 data.append("_token", "{{ Session::token() }}");
		 data.append("_method", "DELETE");
		 r.send(data);

		 // delete comment on client side
		 var commentbox = event.target.parentElement;

		 var newcontent = document.createElement('p');
		 newcontent.className = "comment_content";
		 newcontent.innerHTML = "[Deleted]";

		 while (commentbox.firstChild) {
		     commentbox.removeChild(commentbox.firstChild);
		 }

		 commentbox.appendChild(newcontent);
	     }
	 });
     }
    </script>
    <script type="text/javascript">
     var upvote_btns = document.getElementsByClassName('vote');
     for (i = 0; i < upvote_btns.length; i++) {
	 upvote_btns[i].addEventListener('click', function(event) {
	     var thingtype = event.target.getAttribute('data-type');
	     var thingid = event.target.getAttribute('data-id');

	     var url = (thingtype == "post" ? "/post/" : "/comment/")
		     + thingid
		     + '/vote';

	     var is_upvote = /(?:^|\s)upvote(?:\s|$)/.test(event.target.className);

	     var ajax = new XMLHttpRequest();
	     ajax.open("POST", url, true);
	     var data = new FormData();
	     data.append("_token", "{{ Session::token() }}");
	     data.append("vote",(is_upvote ? "1" : "0"));
	     ajax.send(data);
	     ajax.onload = function () {
		 var response = JSON.parse(ajax.responseText);

		 if (!response.vote_succeded) {
		     // display errors
		     var error_section = document.getElementById("errors");
		     var error_node = document.createElement("b");
		     error_node.classList.add("error");
		     error_node.innerHTML = response.errors;
		     while (error_section.firstChild) {
			 error_section.removeChild(error_section.firstChild);
		     }
		     error_section.appendChild(error_node);
		     window.scrollTo(0,0);
		 } 
		 
		 var amount = 0;
		 if (response.response_type == 1) { amount = 1; }
		 else if (response.response_type == 2) { amount = -1; }
		 else if (response.response_type == 3) { amount = 2; }
		 if (is_upvote) {
		     var vote_counter = event.target
					     .parentElement
					     .parentElement
					     .nextElementSibling
					     .childNodes[1]
					     .childNodes[1];
		     vote_counter.innerHTML = Number(vote_counter.innerHTML) + amount;

		 } else {
		     amount = amount * -1;
		     var vote_counter = event.target
					     .parentElement
					     .parentElement
					     .previousElementSibling
					     .childNodes[1]
					     .childNodes[1];
		     vote_counter.innerHTML = Number(vote_counter.innerHTML) + amount;
		 }
	     }
	 });
     }
    </script>
@endsection
