@extends('layouts.main')

@section('title')
    Gallery
@endsection

@section('content')
    <h1>Gallery:</h1>
    @foreach($attachments as $attachment)
        @if(preg_match('/image\/\w+/', $attachment->type) and $attachment->show_in_gallery)
            <a href="{{url("/post/".$attachment->post_id)}}"><img class="picture" src="{{ url($attachment->path) }}" style="max-width: 200px; max-height: 200px" /></a>
        @endif
    @endforeach

@endsection