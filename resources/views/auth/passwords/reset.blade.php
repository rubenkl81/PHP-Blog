@extends('layouts.main')

@section('title')
    Password Reset
@endsection

@section('content')
    <section id="passwordresetform">
	<h3>Reset password:</h3>
	<form method="POST" action="{{ url('/password/reset') }}">
	    {!! csrf_field() !!}
	    <input type="hidden" name="token" value="{{ $token }}">
	    <table>
		<tr>
		    <th>Email:</th>
		    <td><input type="email" name="email" value="{{ $email or old('/password/reset') }}" /></td>
		</tr>
		<tr>
		    <th>Password:</th>
		    <td><input type="password" name="password" /></td>
		</tr>
		<tr>
		    <th>Confirm Password:</th>
		    <td><input type="password" name="password_confirmation" /></td>
		</tr>
		<tr>
		    <td></td>
		    <td><input type="submit" value="Reset password!" /></td>
		</tr>
	    </table>
	</form>
    </section>
@endsection
