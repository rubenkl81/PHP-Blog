@extends('layouts.main')

@section('title')
    Password Reset
@endsection

@section('content')
    <section id="passwordresetemail">
	<h3>Send password reset email: </h3>
	<form method="POST" action="{{ url('/password/email') }}">
	    {!! csrf_field() !!}
	    <table>
		<tr>
		    <th>Email:</th>
		    <td><input type="email" name="email" value="{{ old('/login') }}" /></td>
		</tr>
		<tr>
		    <td></td>
		    <td><input type="submit" value="Send reset link" /></td>
		</tr>
	    </table>
	</form>
    </section>
@endsection
