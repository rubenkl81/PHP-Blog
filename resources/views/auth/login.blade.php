@extends('layouts.main')

@section('title')
Login
@endsection

@section('content')

    <section id="loginform">
	<h3>Login:</h3>
	<form action="{{ url('/login') }}" method="post">
	    {!! csrf_field() !!}
	    <table>
		<tr>
		    <th>Email:</th>
		    <td><input type="email" name="email" value="{{ old('/login') }}" /></td>
		</tr>
		<tr>
		    <th>Password:</th>
		    <td><input type="password" name="password" /></td>
		</tr>
		<tr>
		    <th><a href="{{ url ('/password/reset') }}">Forgot password?</a></th>
		    <td><input type="checkbox" name="remember" /> Remember me</td>
		</tr>
		<tr>
		    <td></td>
		    <td><input type="submit" value="Login" /></td>
		</tr>
	    </table>
	</form>
    </section>
@endsection
