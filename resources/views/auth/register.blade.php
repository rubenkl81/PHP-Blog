@extends('layouts.main')

@section('title')
    Sign Up
@endsection

@section('content')
    
    <section id="registerform">
	<h3>Sign up:</h3>
	<form action="{{ url('/register') }}" method="post">
	    {!! csrf_field() !!}
	    <table>
		<tr>
		    <th>Name:</th>
		    <td><input type="text" name="name" value="{{ old('/login') }}" /></td>
		</tr>
		<tr>
		    <th>Email:</th>
		    <td><input type="email" name="email" value="{{ old('/login') }}" /></td>
		</tr>
		<tr>
		    <th>Password:</th>
		    <td><input type="password" name="password" /></td>
		</tr>
		<tr>
		    <th>Confirm Password:</th>
		    <td><input type="password" name="password_confirmation" /></td>
		</tr>
		<tr>
		    <td></td>
		    <td><input type="submit" value="Register" /></td>
		</tr>
	    </table>
	</form>
    </section>

@endsection
