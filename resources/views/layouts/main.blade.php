<!DOCTYPE html>
<html>
    <head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>@yield("title")</title>
	<link rel="stylesheet" href="{{ asset("css/app.css") }}">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,600' rel='stylesheet' type='text/css'>
	<link rel="icon" href="{{ asset("favicon.png") }}">
    </head>
    <body>
	@include('header')
	<div class="wrapper">
	    @include('common.errors')
	    @yield("content")
	</div>
	<a id="surprise">click me</a>
	<script type="text/javascript">
	 var surpbtn = document.getElementById('surprise');
	 surpbtn.addEventListener('click', function (event) {
	     /* var elems = document.getElementsByTagName('p');
		for (i = 0; i < elems.length; i++) {
		elems[i].classList.add('shitfaced');
		}
	      */
	     var elems = ["p", "h1", "h2", "b", "a", "ul", "article"].map(function(e) {
		 return Array.prototype.slice.call(document.getElementsByTagName(e))
	     }).map(function (elems) {
		 elems.map(function (elem) {
		     elem.classList.add("shitfaced");
		 });
	     });
	 });
	</script>
    </body>
</html>

