@extends('layouts.main')

@section('content')
    <form @if($update)
 action="{{ route('post.update',[$blog_post->id]) }}" method="post"
    @else
 action="{{ route('post.store') }}" method="post"
    @endif
 id="newpostform"  enctype="multipart/form-data">
	<table class="newpost">
	    <h1>New Post:</h1>
	    {!! csrf_field() !!}
	    @if ($update)
		<input name="_method" value="PUT" type="hidden" />
	    @endif
	    <tr>
		<th>
		    <label for="title">Title:</label>
		</th><td>
		    <input type="text" name="title" id="title" value="{{$blog_post->title or ""}}">
		</td>
	    </tr>
	    <tr>
		<th>
		    <label for="postdata">Text:</label>
		</th>
		<td>
		    <textarea name="postdata">{{$blog_post->postdata or ""}}</textarea>
		</td>
	    </tr>
	    <tr>
		<th>
		    <label for="image">File:</label>
		</th>
		<td>
		    <div id="attachments">
			<input name="images[]" type="file" />
		    </div>
		    <input name="attachment" type="button" id="add_attachment" value="add attachment">
		    @if(isset($blog_post) and count($blog_post->attachments) > 0)
			<table class="edit_attachments">
			    <td>Show in gallery | </td>
			    <td>Attachments:</td>
			    @foreach ($blog_post->attachments as $attachment)
				<input type="hidden" name="attachment_ids[]" value="{{ $attachment->id }}" />
				<tr>
				    <td><input type="checkbox" name="attachments_show_in_gallery_{{ $attachment->id }}"
					       @if($attachment->show_in_gallery)
					       checked="checked"
					       @endif
					></input></td>
				    <td><a href="{{ url($attachment->path) }}">{{ $attachment->path }}</a></td>
				    <td><button class="delete_attachment" type="button" data-parent="{{ $attachment->id }}">Delete</button></td>
				</tr>
			    @endforeach
			</table>
				    @endif
				    </td>
	    </tr>
	    <tr>
		<th></th>
		<td>
		    <input type="submit" value="Post">
		</td>
	    </tr>
	</table>
    </form>
    <script type="text/javascript">
     var btn_add_attachment = document.getElementById("add_attachment");
     btn_add_attachment.addEventListener("click", function() {
	 var file_attachment_upload = document.createElement("input");
	 file_attachment_upload.setAttribute('type','file');
	 file_attachment_upload.setAttribute('name','images[]');
	 var div_attachments = document.getElementById("attachments");
	 div_attachments.appendChild(file_attachment_upload);
     });
    </script>

    <script type="text/javascript">
	var delete_attachment_btns = document.getElementsByClassName('delete_attachment');
	for(i=0; i<delete_attachment_btns.length; i++){
						  delete_attachment_btns[i].addEventListener('click', function (event) {
						  var conf = confirm("Are you sure you want to delete this attachment?");
						  if(conf){
						  var r = new XMLHttpRequest();
						  var id = event.target.getAttribute("data-parent");
						  r.open("POST", "/attachment/" + id + "/", true);
						  r.setRequestHeader('X_CSRF_TOKEN', '{{ Session::token() }}');
						  var data=new FormData();
						  data.append("_token", "{{ Session::token() }}");
						  data.append("_method", "DELETE");
						  r.send(data);

						  event.target.parentElement.parentElement.remove();
	}
						  });
	}
    </script>
@endsection
