<div id="errors">
    @if (count($errors) > 0)
	<h3>You entered something wrong:</h3>

	<ul class="errorlist">
	    @foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	    @endforeach
	</ul>
	
    @endif

    @if (Session::has('message'))
	<b class="error">Error: {{ Session::get('message') }}</b>
    @endif
    {{ Session::forget('message') }}
    {{ Session::forget('errors') }}
</div>
